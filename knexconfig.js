require('dotenv').config();

const knex = require('knex')(
	{
		client: 'mysql',
		connection: {
			host: process.env.DB_HOST,
			port: process.env.DB_PORT,
			user: process.env.DB_USER,
			password: process.env.DB_PASS,
			database: process.env.DB_NAME,
			timezone: '-05:00',
			options: {
				encrypt: false
			},
			pool: {
				afterCreate: function(conn, cb) {
					conn.query('SET sql_mode="NO_ENGINE_SUBSTITUTION";', function (err) {
						cb(err, conn);
					});
				}
			}        
		}
	}
)

module.exports = knex;
