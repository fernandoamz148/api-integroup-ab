module.exports = function(router, dbServer, token, md5, knex) {

    async function getUserData(usuario, password) {
        return new Promise((resolve, reject) => {
            knex
                .select('*')
                .from('catusuarios AS catu')
                .leftJoin('cattipousuarios AS catup', 'catup.id_tipo_usuario', 'catu.id_tipo_usuario')
                .where('catu.usuario_login', '=', usuario)
                .where('catu.password', '=', password)
                .then(result => {
                    const parseResult = JSON.parse(JSON.stringify(result));
                    resolve(parseResult)
                })
                .catch((error) => reject(error))
        });
    }

    async function getUserPermissions(userData) {
        const { id_usuario } = userData[0];

        return new Promise((resolve, reject) => {
            knex.select('*')
                .from('catusuarios AS catu')
                .leftJoin('cattipousuarios AS catup', 'catup.id_tipo_usuario', 'catu.id_tipo_usuario')
                .leftJoin('relusuarioseccion AS relsec', 'relsec.id_tipo_usuario', 'catu.id_tipo_usuario')
                .leftJoin('sissecciones AS sisec', 'sisec.id_seccion', 'relsec.id_seccion')
                .where('catu.id_usuario', '=', id_usuario)
                .then(result => {
                    const parseResult = JSON.parse(JSON.stringify(result));
                    resolve(parseResult)
                })
                .catch(error => reject(error))
        });
    }

    async function saveRelationUserClient(idUser, clientes, res) {
        return new Promise((resolve, reject) => {
            for (let i in clientes) {

                knex('relusuariocliente').insert({
                    id_usuario: idUser,
                    id_cliente: clientes[i].value
                })
                .then(inserted => {
                    if (parseInt(i) === (clientes.length - 1)) {
                        res.json({ status: 'OK', result: "usuario insertado" });
                        resolve(inserted);
                    };
                })
                .catch(error => reject(error))
            }
        });
    }

    async function saveClient(user) {
        const {
            usuario_nombre,
            usuario_login,
            id_tipo_usuario,
            password,
            correo_electronico
        } = user;

        return new Promise((resolve, reject) => {
            knex('catusuarios').insert({
                usuario_nombre,
                usuario_login,
                password: md5(password),
                estatus: 'ACTIVO',
                id_tipo_usuario,
                correo_electronico
            })
            .then(idUser => resolve(idUser[0]))
            .catch(error => reject(error))
        });
    }

    async function existUser(user) {
        const { usuario_login } = user;

        return new Promise((resolve, reject) => {
            knex('catusuarios')
            .select('*')
            .where('usuario_login', '=', usuario_login)
            .then(result => {
                const parseResult = JSON.parse(JSON.stringify(result));
                resolve(parseResult)
            })
            .catch(error => reject(error))
        });
    }

    async function getUserRelation(id_usuario) {
        return new Promise((resolve, reject) => {
            knex('relusuariocliente')
            .select('*')
            .where('id_usuario', '=', id_usuario)
            .then(result => {
                const parseResult = JSON.parse(JSON.stringify(result));
                resolve(parseResult)
            })
            .catch(error => reject(error))
        });
    }

    router.post('/login', async (req, res) => {
        var usuario = req.body.usuario;
        var password = req.body.password;

        const userData = await getUserData(usuario, md5(password));

        if (!userData.length) {
            res.json({ status: 'ERROR', result: 'USUARIO INCORRECTO' });
        } if (userData[0].estatus === 'ACTIVO') {
            
            const userPermissions = await getUserPermissions(userData);

            if (userData[0].id_tipo_usuario === 3) {
                res.json({
                    status: 'OK',
                    result: {
                        id_usuario: userData[0].id_usuario,
                        usuario_nombre: userData[0].usuario_nombre,
                        estatus: userData[0].estatus,
                        correo_electronico: userData[0].correo_electronico,
                        id_tipo_usuario: 3,
                        secciones: userPermissions
                    }
                });
            } else {
                res.json({
                    status: 'OK',
                    result: {
                        id_usuario: userData[0].id_usuario,
                        usuario_nombre: userData[0].usuario_nombre,
                        estatus: userData[0].estatus,
                        correo_electronico: userData[0].correo_electronico,
                        secciones: userPermissions 
                    }
                });
            }
        }
    });

    router.post('/crear/usuario', async (req, res) => {
        const {
            tokenUser,
            id_tipo_usuario,
            clientes
        } = req.body;

        if (token === tokenUser) {

            const userExist =  await existUser(req.body);
            
            if (userExist.length) {
                res.json({ status: 'ERROR', result: userExist });
            } else {
                const id_usuario = await saveClient(req.body);

                if (id_tipo_usuario === 3) {
                    await saveRelationUserClient(id_usuario, clientes, res);
                } 
            }

        } else {
            res.json({ status: 'ERROR', result: 'token invalido' });
        }
    });

    router.post('/actualizar/usuario', (req, res, next) => {
        let tokenUser   = req.body.tokenUser
        let id_usuario  = req.body.id_usuario
        let usuario_nombre = req.body.usuario_nombre
        let usuario_login = req.body.usuario_login
        let id_tipo_usuario = req.body.id_tipo_usuario 
        let password    = req.body.password
        let correo_electronico = req.body.correo_electronico

        if(token === tokenUser){
            let update =     
                `   
                    UPDATE catusuarios
                    SET usuario_nombre = "${usuario_nombre}", 
                        usuario_login = "${usuario_login}",
                        id_tipo_usuario = ${id_tipo_usuario},
                        correo_electronico = "${correo_electronico}"
                `
            
            if(password){
                update += `,password = "${md5(password)}"`
            }

            update += ` WHERE id_usuario = ${id_usuario}`
            
            dbServer.query(update, { type: dbServer.QueryTypes.UPDATE})
            .then(result => {
                res.json({ status: 'OK', result: result })
            })
            .catch(error => {
                res.json({ status: 'ERROR', result: error })
            })
        }
    })

    router.get('/buscar/usuario/:id/:tokenUser', (req, res, next) => { 
        let tokenUser = req.params.tokenUser

        if(token === tokenUser){ 
            const consulta = `SELECT * FROM catusuarios WHERE id_usuario = ${req.params.id}`

            dbServer.query(consulta, { type: dbServer.QueryTypes.SELECT })
            .then(result => {
                if (result[0].id_tipo_usuario === 3) {
                    const consulta_relacion = `
                        SELECT catcli.razon_social
                        FROM catusuarios catu
                        LEFT JOIN relusuariocliente relusu ON relusu.id_usuario = catu.id_usuario
                        LEFT JOIN catclientes catcli ON catcli.id_cliente = relusu.id_cliente
                        WHERE catu.id_usuario = ${req.params.id}`

                    dbServer.query(consulta_relacion, { type: dbServer.QueryTypes.SELECT })
                    .then(relacion => {
                        res.json({ statu: 'OK', result: result, relacion_cliente: relacion })
                    })
                    .catch(error => res.json({ statu: 'ERROR', result: error }))
                } else {
                    res.json({ statu: 'OK', result: result })
                }
            })
            .catch(error => { 
                res.json({ status: 'ERROR', result: error })
            })

        }else{ 
            res.json({ status: 'OK', result: 'No has iniciado sesión' })
        }
    })

    router.post('/consulta/usuario', (req, res, next) => {
        var tokenUser   = req.body.tokenUser
       
        if(token === tokenUser){
            
            var queryUsuarios =     
                `   
                    SELECT 
                        catu.id_usuario,
                        catu.usuario_nombre,
                        catu.usuario_login,
                        catu.correo_electronico,
                        catu.id_tipo_usuario,
                        catu.estatus
                    FROM catusuarios catu
                `

            dbServer.query(queryUsuarios, { type: dbServer.QueryTypes.SELECT})
            .then(result => {
                res.json({ status: 'OK', result: result })
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })  
        }
    })

    router.post('/eliminar/usuario', (req, res, next) => {
        var tokenUser   = req.body.tokenUser
        var id_usuario = req.body.id_usuario

        if(token === tokenUser){
            
            var queryUsuarios =     
                `   
                    UPDATE catusuarios
                    SET estatus = 'INACTIVO'
                    WHERE id_usuario = ${id_usuario}
                `

            dbServer.query(queryUsuarios, { type: dbServer.QueryTypes.UPDATE })
            .then(result => {
                res.json({ status: 'OK', result: result })
            })
            .catch(error => {
                res.json({ status: 'ERROR', result: error })
            })  
        }
    })

    router.post('/activar/usuario', (req, res, next) => {
        var tokenUser   = req.body.tokenUser
        var id_usuario = req.body.id_usuario

        if(token === tokenUser){
            
            var queryUsuarios =     
                `   
                    UPDATE catusuarios
                    SET estatus = 'ACTIVO'
                    WHERE id_usuario = ${id_usuario}
                `

            dbServer.query(queryUsuarios, { type: dbServer.QueryTypes.UPDATE })
            .then(result => {
                res.json({ status: 'OK', result: result })
            })
            .catch(error => {
                res.json({ status: 'ERROR', result: error })
            })  
        }
    })
}
