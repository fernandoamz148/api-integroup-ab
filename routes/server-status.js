module.exports = function (router, dbServer, token, knex){
    router.get('/', (req, res, next) => {
        res.json({status: 'OK'})
    })

    router.get('/status/db', (req, res, next) => {
        dbServer
        .authenticate()
        .then(() => {
            res.json({ status: 'OK', dbRes: 'conectado' })
        })
        .catch(err => {
            console.log('[ API ] -> ', err)
            res.json({ status: 'ERROR', dbRes: err })
        })
    });
}
