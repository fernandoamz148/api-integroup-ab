module.exports = function(router, dbServer, token, md5, fs, PDFDocument, moment, knex, uniqid, axios) {

  router.get('/consultar/pagos/:id_usuario/:tokenUser', (req, res, next) => {
    const tokenUser = req.params.tokenUser 
    const id_usuario = req.params.id_usuario 

    if (tokenUser === token){
        knex('relpagosclientes AS repacli')
        .select('*')
        .leftJoin('bitconstancia AS bitcon', 'bitcon.id_constancia', 'repacli.id_constancia')
        .leftJoin('catclientes AS catcli', 'catcli.id_cliente', 'repacli.id_cliente')
        .where('repacli.id_usuario', '=', id_usuario)
        .where('repacli.id_ticket', 'IS', null)
        .where('repacli.status', '=', 'PENDIENTE')
        .then(result => {
          res.json({status: 'OK', result: result})
        })
        .catch(error => {
          res.json({status: 'ERROR', result: error})
        })
    } else {
        res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
    }
  });

  router.get('/consultar/pagos/:tokenUser', (req, res, next) => {
    const tokenUser = req.params.tokenUser 
    const id_usuario = req.params.id_usuario 

    if (tokenUser === token){   
      knex('relpagosclientes AS repacli')
      .select('*')
      .leftJoin('bitconstancia AS bitcon', 'bitcon.id_constancia', 'repacli.id_constancia')
      .leftJoin('catclientes AS catcli', 'catcli.id_cliente', 'repacli.id_cliente')
      .where('repacli.id_ticket', 'IS', null)
      .where('repacli.status', '=', 'PENDIENTE')
      .then(result => {
        res.json({status: 'OK', result: result})
      })
      .catch(error => {
        res.json({status: 'ERROR', result: error})
      })
    } else {
      res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
    }
  });

  router.post('/enviar/facturas', async (req, res, next) => {
    const {
      tokenUser,
      polizas,
      totalprice,
      valorUnitario,
    } = req.body;
      
      console.log(polizas)
      if (tokenUser === token) {
        const uniqueId = uniqid();
        const auxiliarId = uniqid();
        const cantidad = polizas.length;

        updatePolizas(polizas, uniqueId, auxiliarId)
        .then(async (response) => {
          console.log(response)

          const descripcion = await readDescription(auxiliarId, polizas);

          const data = JSON.stringify({
            "entity": {
              "data": {
                "claveAcceso": "avXjTd5EUi5Y91BiY2aB",
                "numTicket": uniqueId,
                "auxId": auxiliarId,
                "moneda": "MXN",
                "formaDePago": "_03",
                "sucursal": {
                  "id": 28594
                },
                "partidas": [{
                  "descripcion": descripcion,
                  "valorUnitario": valorUnitario,
                  "claveUnidad": "E48",
                  "claveProdServ": "84131500",
                  "cantidad": cantidad,
                  "tasaIvaTrasladado": 0.16,
                }]
              }
            }
          });
          
          const config = {
            method: 'post',
            url: 'https://app.facture.com.mx/api/autofactura',
            headers: { 
              'Authorization': '725c6dfed7115354c30efb9d0b1af00f', 
              'Accept': 'application/json', 
              'Content-Type': 'application/json'
            },
            data: data
          }; 

          axios(config)
            .then((response) => {
              console.log(response.data)
              res.json({status: 'OK', result: polizas })
            })
            .catch(error => error);
         
        })
    } else {
      res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
    }
  });

  router.get('/obtener/tickets/:tokenUser', (req, res, next) => {
    const {
      tokenUser,
    } = req.params;

    if (tokenUser === token) {
      
      console.log(knex('relpagosclientes AS repacli')
      .select('repacli.*', 'bitcon.*', 'catcli.*')
      .sum('bitcon.prima_neta_total AS suma')
      .leftJoin('bitconstancia AS bitcon', 'bitcon.id_constancia', 'repacli.id_constancia')
      .leftJoin('catclientes AS catcli', 'catcli.id_cliente', 'bitcon.id_cliente')
      .where('repacli.id_ticket', 'IS NOT', null)
      .groupBy('repacli.id_ticket')
      .groupBy('repacli.id_aux')
      .orderBy('repacli.timestamp', 'desc').toSQL().toNative())

      knex.schema.raw("SET sql_mode='TRADITIONAL'")
      .then((queryschema) => {
        knex('relpagosclientes AS repacli')
          .select('repacli.*', 'bitcon.*', 'catcli.*')
          .sum('bitcon.prima_neta_total AS suma')
          .leftJoin('bitconstancia AS bitcon', 'bitcon.id_constancia', 'repacli.id_constancia')
          .leftJoin('catclientes AS catcli', 'catcli.id_cliente', 'bitcon.id_cliente')
          .where('repacli.id_ticket', 'IS NOT', null)
          .groupBy('repacli.id_ticket')
          .groupBy('repacli.id_aux')
          .orderBy('repacli.timestamp', 'desc')
          .then(result => {
            res.json({ status: 'OK', result })
          })
          .catch(error => {
            res.json({ status: 'ERROR', result: error })
          });
      })

    } else {
      res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
    }
  
  });

  router.post('/facturado', (req, res, next) => {
    const {
      polizas,
    } = req.body;

    if (token === req.body.token) {
      polizas.map(poliza => {
        knex('relpagosclientes')
          .where({
            id_constancia: poliza.id_constancia,
          })
          .update({
            status: 'FACTURADO_OTRO_MEDIO',
            timestamp_ticket: knex.fn.now(),
          })
          .then(response => {
            return res.json({ status: 'OK', result: response })
          })
          .catch(error => {
            return res.json({ status: 'ERROR', result: error })
          });
      })
    } else {
      res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
    };
  });

  async function updatePolizas(polizas, uniqueId, auxiliarId) {
    return (
      polizas.map(async (poliza) => {
        await knex('relpagosclientes')
          .where({
            id_constancia: poliza.id_constancia,
          })
          .update({
            id_ticket: uniqueId,
            id_aux: auxiliarId,
            timestamp_ticket: knex.fn.now(),
            status: 'ENVIADO',
          })
          .then(response => response)
          .catch(error => error);
        })
    )
  }

  async function readDescription(auxiliarId, polizas) {
    return (
      knex.raw(
        `
          SELECT CONCAT("Asesoría en Seguros de Mercancías", " ", GROUP_CONCAT(bitcons.no_embarque)) AS descripcion
          FROM relpagosclientes repagcli
          LEFT JOIN bitconstancia bitcons ON bitcons.id_constancia = repagcli.id_constancia
          WHERE repagcli.id_aux = '${auxiliarId}'
        `
      )
      .then(result => {
        const isDescripcion = result[0][0].descripcion;

        if (!isDescripcion) {
          return `Asesoría en Seguros de Mercancía ${polizas[0].no_embarque}`
        }

        return result[0][0].descripcion
      })
      .catch(error => error)
    );
  };
};
