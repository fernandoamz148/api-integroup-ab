module.exports = function(router, dbServer, token, md5){

    router.post('/crear/cliente', (req, res, next) => {
        var tokenUser   = req.body.tokenUser
        var nombre = req.body.nombre
        var apellido_paterno = req.body.apellido_paterno
        var apellido_materno = req.body.apellido_materno
        var rfc = req.body.rfc
        var razon_social = req.body.razon_social
        var correo_electronico = req.body.correo_electronico
        var id_tipo_persona = req.body.id_tipo_persona
        var calle = req.body.calle
        var no_exterior = req.body.no_exterior
        var no_interior = req.body.no_interior
        var colonia = req.body.colonia
        var id_pais = req.body.id_pais
        var id_estado = req.body.id_estado
        var id_municipio = req.body.id_municipio
        var codigo_postal = req.body.codigo_postal
        var cuota = req.body.cuota
        var deducibleUno = req.body.deducibleUno
        var deducibleDos = req.body.deducibleDos
        var deducibleTres = req.body.deducibleTres
        var tipo_compania = req.body.tipo_compania
        var nombre_agrupador = req.body.nombre_agrupador

        if(token === tokenUser){
            var queryClientes =     
                `   
                INSERT INTO 
                    catclientes
                    (
                        nombre,
                        apellido_paterno,
                        apellido_materno,
                        rfc,
                        razon_social,
                        correo_electronico,
                        id_tipo_persona,
                        calle,
                        no_exterior,
                        no_interior,
                        colonia,
                        id_pais,
                        id_estado,
                        id_municipio,
                        codigo_postal,
                        fecha_creacion,
                        cuota,
                        deducible1,
                        deducible2,
                        deducible3,
                        tipo_compania,
                        nombre_agrupador
                    )
                    VALUES
                    (
                        "${nombre}",
                        "${apellido_paterno}",
                        "${apellido_materno}",
                        "${rfc}",
                        "${razon_social}",
                        "${correo_electronico}",
                        `+(id_tipo_persona ? id_tipo_persona : 0)+`,
                        "${calle}",
                        "${no_exterior}",
                        "${no_interior}",
                        "${colonia}",
                        `+(id_pais ? id_estado : 0)+`,
                        `+(id_estado ? id_estado : 0)+`,
                        `+(id_municipio ? id_municipio : 0)+`,
                        `+(codigo_postal ? codigo_postal: 0)+`,
                        CURRENT_TIMESTAMP,
                        `+(cuota ? cuota : 0)+`,
                        "${deducibleUno}",
                        "${deducibleDos}",
                        "${deducibleTres}",
                        "${tipo_compania}",
                        "${nombre_agrupador}"
                    )
                `    
            dbServer.query(queryClientes, { type: dbServer.QueryTypes.INSERT})
            .then(result => {
                res.json({status: 'OK', result: result})
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })  
        }
    })

    router.post('/actualizar/cliente', (req, res, next) => {
        var tokenUser   = req.body.tokenUser
        var id_cliente = req.body.id_cliente
        var nombre = req.body.nombre
        var apellido_paterno = req.body.apellido_paterno
        var apellido_materno = req.body.apellido_materno
        var rfc = req.body.rfc
        var razon_social = req.body.razon_social
        var correo_electronico = req.body.correo_electronico
        var id_tipo_persona = req.body.id_tipo_persona
        var calle = req.body.calle
        var no_exterior = req.body.no_exterior
        var no_interior = req.body.no_interior
        var colonia = req.body.colonia
        var id_pais = req.body.id_pais
        var id_estado = req.body.id_estado
        var id_municipio = req.body.id_municipio
        var codigo_postal = req.body.codigo_postal
        var cuota = req.body.cuota
        var deducibleUno = req.body.deducibleUno
        var deducibleDos = req.body.deducibleDos
        var deducibleTres = req.body.deducibleTres
        var tipo_compania = req.body.tipo_compania
        var nombre_agrupador = req.body.nombre_agrupador

        if(token === tokenUser){
            var queryClientes =     
                `    
                    UPDATE catclientes
                    SET nombre ="${nombre}",
                        apellido_paterno = "${apellido_paterno}",
                        apellido_materno = "${apellido_materno}",
                        rfc = "${rfc}",
                        razon_social = "${razon_social}",
                        correo_electronico = "${correo_electronico}",
                        id_tipo_persona = `+id_tipo_persona+`,
                        calle = "${calle}",
                        no_exterior = "${no_exterior}",
                        no_interior = "${no_interior}",
                        colonia = "${colonia}",
                        id_pais = `+id_pais+`,
                        id_estado = `+id_estado+`,
                        id_municipio = `+id_municipio+`,
                        codigo_postal = `+codigo_postal+`,
                        cuota = `+cuota+`,
                        ultima_actualizacion = CURRENT_TIMESTAMP,
                        deducible1 = "${deducibleUno}",
                        deducible2 = "${deducibleDos}",
                        deducible3 = "${deducibleTres}",
                        tipo_compania = "${tipo_compania}",
                        nombre_agrupador = "${nombre_agrupador}"
                    WHERE id_cliente = `+id_cliente+`
                `            
            dbServer.query(queryClientes, { type: dbServer.QueryTypes.UPDATE})
            .then(result => {
                res.json({status: 'OK', result: result})
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })  
        }
    })

    router.post('/eliminar/cliente', (req, res, next) => {
        var tokenUser   = req.body.tokenUser
        var id_cliente = req.body.id_cliente

        if(token === tokenUser){
            var queryClientes =     
                `    
                    DELETE FROM catclientes WHERE id_cliente = ${id_cliente}
                `                    
            dbServer.query(queryClientes, { type: dbServer.QueryTypes.DELETE})
            .then(result => {
                res.json({status: 'OK', result: result})
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })  
        }
    })

    router.get('/actualizar/cliente/:id_cliente/:tokenUser', (req, res, next) => {
        var id_cliente = req.params.id_cliente
        var tokenUser = req.params.tokenUser

        if(token === tokenUser){
            var consulta = 
            `
                SELECT * FROM catclientes WHERE id_cliente = ${id_cliente}
            `

            dbServer.query(consulta, { type: dbServer.QueryTypes.SELECT })
            .then(result => {
                res.json({status: 'OK', result: result })
            })
            .catch(err => {
                res.json({status: 'ERROR', result: err })
            })
        }
        
    })
}
