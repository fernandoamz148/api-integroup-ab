module.exports = function(router, dbServer, token, md5, fs, PDFDocument, moment, knex){

    router.get('/consultar/companias/:tokenUser', (req, res, next) => {
        var tokenUser = req.params.tokenUser 
    
        if(tokenUser === token){
            knex('cattipocompania')
            .then(result => {
                res.json({status: 'OK', result: result})
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })
        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })

    router.get('/consultar/companias/:id_tipo_compania/:tokenUser', (req, res, next) => {
        var tokenUser = req.params.tokenUser 
        var id_tipo_compania = req.params.id_tipo_compania

        if(tokenUser === token){
            knex('cattipocompania').where('id_tipo_compania', id_tipo_compania)
            .then(result => {
                res.json({status: 'OK', result: result})
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })
        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })

    router.post('/actualizar/compania', (req, res, next) => {
        var tokenUser = req.body.tokenUser 
        var id_tipo_compania = req.body.id_tipo_compania
        var tipo_compania = req.body.tipo_compania
        var clausulas = req.body.clausulas

        if(tokenUser === token){
            
            knex('cattipocompania')
            .where('id_tipo_compania', '=', id_tipo_compania)
            .update({
              tipo_compania,
              clausulas
            })
            .then(result => {
                res.json({ status: 'OK', result: result })
            })
            .catch(error => {
                res.json({ status: 'ERROR', result: error })
            });

        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })

    router.post('/crear/compania', (req, res, next) => {
      var tokenUser = req.body.tokenUser 
      var tipo_compania = req.body.tipo_compania
      var clausulas = req.body.clausulas

      if(tokenUser === token){

        knex('cattipocompania').insert({
          tipo_compania,
          clausulas
        })
        .then(result => {
            res.json({ status: 'OK', result: result })
        })
        .catch(error => {
            res.json({ status: 'ERROR', result: error })
        });

      }else{
        res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
      }
    })

    router.post('/eliminar/compania', (req, res, next) => {
        var tokenUser = req.body.tokenUser 
        var id_tipo_compania = req.body.id_tipo_compania

        if(tokenUser === token){

            knex('cattipocompania')
            .where('id_tipo_compania', id_tipo_compania)
            .del()
            .then(result => {
                res.json({status: 'OK', result: result})
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })
        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })
}
