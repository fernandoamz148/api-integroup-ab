module.exports = function(router, dbServer, token, md5, fs, PDFDocument, moment, excel, knex){

    router.get('/consultar/clientes/:tokenUser', (req, res, next) => {
        var tokenUser = req.params.tokenUser

        if(tokenUser === token){
            var queryUsuarios =
                `
                    SELECT
                        *
                    FROM catclientes
                `
            dbServer.query(queryUsuarios, { type: dbServer.QueryTypes.SELECT})
            .then(result => {
                res.json({status: 'OK', result: result})
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })
        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })

    router.get('/consultar/clientes/:id_cliente/:tokenUser', (req, res, next) => {
        var id_cliente = req.params.id_cliente
        var tokenUser = req.params.tokenUser

        if(tokenUser === token){
            var queryUsuarios =
                `
                    SELECT
                        cclient.*,
                        pai.nombre as pais,
                        est.estado,
                        muni.municipio
                    FROM catclientes cclient
                    LEFT JOIN paises pai ON pai.id_pais = cclient.id_pais
                    LEFT JOIN estados est ON est.id = cclient.id_estado
                    LEFT JOIN municipios muni ON muni.id = cclient.id_municipio
                    WHERE id_cliente = `+id_cliente+`
                `
            dbServer.query(queryUsuarios, { type: dbServer.QueryTypes.SELECT})
            .then(result => {
                res.json({status: 'OK', result: result})
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })
        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })

    router.get('/consultar/clientes/usuario/:id_usuario/:tokenUser', (req, res, next) => {
        var id_usuario = req.params.id_usuario
        var tokenUser = req.params.tokenUser

        if(tokenUser === token){
            var queryUsuarios =
                `
                    SELECT
                        cclient.*,
                        pai.nombre as pais,
                        est.estado,
                        muni.municipio
                    FROM catclientes cclient
                    LEFT JOIN paises pai ON pai.id_pais = cclient.id_pais
                    LEFT JOIN estados est ON est.id = cclient.id_estado
                    LEFT JOIN municipios muni ON muni.id = cclient.id_municipio
                    LEFT JOIN relusuariocliente relusu ON relusu.id_cliente = cclient.id_cliente
                    WHERE relusu.id_usuario = `+id_usuario+`
                `
            dbServer.query(queryUsuarios, { type: dbServer.QueryTypes.SELECT })
            .then(result => {
                res.json({status: 'OK', result: result })
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })
        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })


    router.get('/consultar/bienes/asegurados/:tokenUser', (req, res, next) => {
        var tokenUser = req.params.tokenUser

        if(tokenUser === token){
            var queryUsuarios =
                `
                    SELECT id_bien_asegurado, bien_asegurado
                    FROM catbienesasegurados
                `
            dbServer.query(queryUsuarios, { type: dbServer.QueryTypes.SELECT})
            .then(result => {
                res.json({status: 'OK', result: result})
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })
        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })

    router.get('/consultar/tipo/embarque/:tokenUser', (req, res, next) => {
        var tokenUser = req.params.tokenUser

        if(tokenUser === token){
            var queryUsuarios =
                `
                    SELECT id_tipo_embarque, tipo_embarque
                    FROM cattipoembarque
                `
            dbServer.query(queryUsuarios, { type: dbServer.QueryTypes.SELECT})
            .then(result => {
                res.json({status: 'OK', result: result})
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })
        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })

    router.get('/consultar/tipo/moneda/:tokenUser', (req, res, next) => {
        var tokenUser = req.params.tokenUser

        if(tokenUser === token){
            var queryUsuarios =
                `
                    SELECT id_tipo_moneda, tipo_moneda
                    FROM cattipomoneda
                `
            dbServer.query(queryUsuarios, { type: dbServer.QueryTypes.SELECT})
            .then(result => {
                res.json({status: 'OK', result: result})
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })
        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })

    router.get('/consultar/medio/transporte/:tokenUser', (req, res, next) => {
        var tokenUser = req.params.tokenUser

        if(tokenUser === token){
            var queryUsuarios =
                `
                    SELECT id_medio_transporte, medio_transporte
                    FROM catmediotransporte
                `
            dbServer.query(queryUsuarios, { type: dbServer.QueryTypes.SELECT})
            .then(result => {
                res.json({status: 'OK', result: result})
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })
        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })

    router.get('/consultar/catalogo/paises/:tokenUser', (req, res, next) => {
        var tokenUser = req.params.tokenUser

        if(tokenUser === token){
            var queryUsuarios =
                `
                    SELECT id_pais, nombre
                    FROM paises
                `
            dbServer.query(queryUsuarios, { type: dbServer.QueryTypes.SELECT})
            .then(result => {
                res.json({status: 'OK', result: result})
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })
        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })

    router.get('/consultar/catalogo/estados/:id_pais/:tokenUser', (req, res, next) => {
        var tokenUser = req.params.tokenUser
        var id_pais = req.params.id_pais

        if(tokenUser === token){
            var queryUsuarios =
                `
                    SELECT id, estado
                    FROM estados
                    WHERE id_pais = `+id_pais+`
                `
            dbServer.query(queryUsuarios, { type: dbServer.QueryTypes.SELECT})
            .then(result => {
                res.json({status: 'OK', result: result})
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })
        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })

    router.get('/consultar/estado/municipio/:estado_id/:tokenUser', (req, res, next) => {
        var tokenUser = req.params.tokenUser
        var estado_id = req.params.estado_id

        if(tokenUser === token){
            var queryUsuarios =
                `
                    SELECT DISTINCT mun.id, mun.municipio
                    FROM estados_municipios  esmu
                    LEFT JOIN municipios mun ON mun.id = esmu.municipios_id
                    WHERE esmu.estados_id = ${estado_id}
                `
            dbServer.query(queryUsuarios, { type: dbServer.QueryTypes.SELECT})
            .then(result => {
                res.json({status: 'OK', result: result})
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })
        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })

    router.post('/crear/constancia', (req, res, next) => {
        const { 
            tokenUser,
            id_usuario,
            id_cliente,
            id_tipo_persona_beneficiario,
            id_bien_asegurado,
            id_tipo_embarque,
            id_tipo_moneda,
            id_medio_transporte,
            id_pais_origen,
            id_estado_origen,
            otro_estado_origen,
            id_ciudad_origen,
            otro_municipio_origen,
            id_pais_destino,
            id_estado_destino,
            otro_estado_destino,
            id_ciudad_destino,
            otro_municipio_destino,
            nombre_beneficiario,
            bienes_beneficiario,
            prima_neta_total,
            otros_gastos,
            iva,
            prima_total,
            detalle_del_bien,
            valor_del_embarque,
            no_embarque,
            transportista,
            rastreo_satelital,
            custodia,
            fecha_salida,
            fecha_aproximada_llegada,
            bmostrar_prima,
            id_tipo_compania,
            tipo_compania,
            nombre_agrupador,
            ncertprov
        } = req.body;

        if(tokenUser === token){
            knex('bitconstancia').insert({
                id_usuario,
                id_cliente,
                id_tipo_persona_beneficiario,
                id_bien_asegurado,
                id_tipo_embarque,
                id_tipo_moneda,
                id_medio_transporte,
                id_pais_origen,
                id_estado_origen,
                otro_estado_origen,
                id_ciudad_origen,
                otro_municipio_origen,
                id_pais_destino,
                id_estado_destino,
                otro_estado_destino,
                id_ciudad_destino,
                otro_municipio_destino,
                nombre_beneficiario,
                bienes_beneficiario,
                prima_neta_total,
                otros_gastos,
                iva,
                prima_total,
                detalle_del_bien,
                valor_del_embarque,
                no_embarque,
                transportista,
                rastreo_satelital,
                custodia,
                fecha_salida,
                fecha_aproximada_llegada,
                bmostrar_prima,
                id_tipo_compania,
                tipo_compania,
                nombre_agrupador,
                estatus_constancia: 'ACTIVA',
                ncertprov
              })
              .then(res_operacion => {
                
                knex('relpagosclientes').insert({
                    id_usuario,
                    id_cliente,
                    id_constancia: res_operacion[0],
                    status: 'PENDIENTE',
                }).then(res_relation => {
                    res.json({ status: 'OK', result: res_operacion });
                }).catch(err_relation => {
                    res.json({ status: 'ERROR', result: err_relation })
                });
              })
              .catch(err_operacion => {
                res.json({ status: 'ERROR', result: err_operacion })
              });
        } else {
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })

    router.get('/crear/constancia/pdf/:id_constancia/:tokenUser', (req, res, next) => {
        var tokenUser = req.params.tokenUser
        var id_constancia = req.params.id_constancia

        if(tokenUser === token){

            var queryPDF =
            `
                SELECT
                    bcons.id_constancia,
                    bcons.bmostrar_prima,
                    causer.usuario_nombre,
                    ccli.nombre,
                    ccli.apellido_paterno,
                    ccli.apellido_materno,
                    ccli.razon_social,
                    ccli.rfc,
                    ccli.calle,
					ccli.no_exterior,
                    ccli.no_interior,
                    ccli.colonia,
                    ccli.deducible1,
					ccli.deducible2,
					ccli.deducible3,
                    estaccli.estado AS estado_cliente,
                    municcli.municipio AS municipio_cliente,
                    ccli.codigo_postal,
                    ctip.tipo_persona,
                    ctipcli.tipo_persona AS tipo_persona_cliente,
                    cbiena.bien_asegurado,
                    cemba.tipo_embarque,
                    ctimon.tipo_moneda,
                    cmeditran.medio_transporte,
                    pai.nombre AS pais_origen,
                    esta.estado,
                    bcons.otro_estado_origen,
                    muni.municipio,
                    bcons.otro_municipio_origen,
                    pais.nombre AS pais_destino,
                    estad.estado AS estado_destino,
                    bcons.otro_estado_destino,
                    munic.municipio AS municipio_destino,
                    bcons.otro_municipio_destino,
                    bcons.nombre_beneficiario,
                    bcons.bienes_beneficiario,
                    bcons.prima_neta_total,
                    bcons.otros_gastos,
                    bcons.iva,
                    bcons.prima_total,
                    bcons.detalle_del_bien,
                    bcons.valor_del_embarque,
                    bcons.no_embarque,
                    bcons.transportista,
                    bcons.rastreo_satelital,
                    bcons.custodia,
                    bcons.fecha_salida,
                    bcons.fecha_aproximada_llegada,
                    cticom.clausulas
                FROM bitconstancia bcons
                LEFT JOIN catusuarios causer ON causer.id_usuario = bcons.id_usuario
                LEFT JOIN catclientes ccli ON ccli.id_cliente = bcons.id_cliente
                LEFT JOIN cattipopersona ctipcli ON ctipcli.id_tipo_persona = ccli.id_tipo_persona
                LEFT JOIN cattipopersona ctip ON ctip.id_tipo_persona = bcons.id_tipo_persona_beneficiario
                LEFT JOIN catbienesasegurados cbiena ON cbiena.id_bien_asegurado = bcons.id_bien_asegurado
                LEFT JOIN cattipoembarque cemba ON cemba.id_tipo_embarque = bcons.id_tipo_embarque
                LEFT JOIN cattipomoneda ctimon ON ctimon.id_tipo_moneda = bcons.id_tipo_moneda
                LEFT JOIN catmediotransporte cmeditran ON cmeditran.id_medio_transporte = bcons.id_medio_transporte
                LEFT JOIN paises pai ON pai.id_pais = bcons.id_pais_origen
                LEFT JOIN estados estaccli ON estaccli.id = ccli.id_estado
                LEFT JOIN estados esta ON esta.id = bcons.id_estado_origen
                LEFT JOIN municipios muni ON muni.id = bcons.id_ciudad_origen
                LEFT JOIN municipios municcli ON municcli.id = ccli.id_municipio
                LEFT JOIN paises pais ON pais.id_pais = bcons.id_pais_destino
                LEFT JOIN estados estad ON estad.id = bcons.id_estado_destino
                LEFT JOIN municipios munic ON munic.id = bcons.id_ciudad_destino
                LEFT JOIN cattipocompania cticom ON cticom.id_tipo_compania = bcons.id_tipo_compania
                WHERE id_constancia = `+id_constancia+`
            `

            dbServer.query(queryPDF, { type: dbServer.QueryTypes.SELECT})
            .then(result => {

                var id_constancia               = pad(result[0].id_constancia,5);
                var usuario_nombre              = result[0].usuario_nombre;
                var nombre                      = result[0].nombre;
                var apellido_paterno            = result[0].apellido_paterno;
                var apellido_materno            = result[0].apellido_materno;
                var tipo_persona_beneficiario   = result[0].tipo_persona
                var razon_social                = result[0].razon_social;
                var rfc                         = result[0].rfc;
                var codigo_postal               = result[0].codigo_postal;
                var calle                       = result[0].calle
                var no_exterior                 = result[0].no_exterior
                var no_interior                 = result[0].no_interior
                var colonia                     = result[0].colonia
                var estado_cliente              = result[0].estado_cliente
                var municipio_cliente           = result[0].municipio_cliente
                var tipo_moneda                 = result[0].tipo_moneda
                var bien_asegurado              = result[0].bien_asegurado
                var tipo_embarque               = result[0].tipo_embarque
                var tipo_persona_cliente        = result[0].tipo_persona_cliente
                var medio_transporte            = result[0].medio_transporte
                var pais_origen                 = result[0].pais_origen
                var estado_origen               = result[0].estado
                var otro_estado_origen          = result[0].otro_estado_origen
                var municipio_origen            = result[0].municipio
                var otro_municipio_origen       = result[0].otro_municipio_origen
                var pais_destino                = result[0].pais_destino
                var estado_destino              = result[0].estado_destino
                var otro_estado_destino         = result[0].otro_estado_destino
                var municipio_destino           = result[0].municipio_destino
                var otro_municipio_destino      = result[0].otro_municipio_destino
                var nombre_beneficiario         = result[0].nombre_beneficiario
                var bienes_beneficiario         = result[0].bienes_beneficiario
                var prima_neta_total            = result[0].prima_neta_total
                var otros_gastos                = result[0].otros_gastos
                var iva                         = result[0].iva
                var prima_total                 = result[0].prima_total
                var detalle_del_bien            = result[0].detalle_del_bien
                var valor_del_embarque          = result[0].valor_del_embarque
                var no_embarque                 = result[0].no_embarque
                var transportista               = result[0].transportista
                var rastreo_satelital           = result[0].rastreo_satelital
                var custodia                    = result[0].custodia
                var mostrar_prima               = result[0].bmostrar_prima
                var fecha_salida                = moment(result[0].fecha_salida).utc().format("DD/MM/YYYY")
                var fecha_actual                = moment().format('MM/DD/YYYY hh:mm A')
                var fecha_aproximada_llegada    = moment(result[0].fecha_aproximada_llegada).utc().format("DD/MM/YYYY")
                var fecha                       = moment().format("DDMMYYYY")
                var title                       = ("constancia_aseguramiento_"+id_constancia+"_"+fecha)
                var filename                    = encodeURIComponent(title) + '.pdf';
                var clausulas                   = result[0].clausulas
                var deducibleUno                = result[0].deducible1
                var deducibleDos                = result[0].deducible2
                var deducibleTres               = result[0].deducible3

                const doc = new PDFDocument({
                    margins : {
                        top: 72,
                        left: 72,
                        right: 27,
                        bottom: 20
                    },
                    bufferPages: true
                });

                res.setHeader('Content-disposition', 'filename="' + filename + '"; inline');
                res.setHeader('Content-type', 'application/pdf');

                doc.registerFont('Arial', 'static/fonts/arial.ttf')
                doc.registerFont('ArialBold', 'static/fonts/Arial-Bold.ttf')

                doc.rect(20, 100, 567, 18)
                    .fillAndStroke("#18244b", "#18244b")

                doc.font('ArialBold', 9)
                    .fill("white")
                    .fontSize(10)
                    .text('NO. DE CONSTANCIA DE ASEGURAMIENTO: ' + id_constancia, 22, 104);

                doc.rect(20, 135, 567, 18)
                    .fillAndStroke("#18244b", "#18244b")

                doc.font('ArialBold', 9)
                    .fill("white")
                    .fontSize(10)
                    .text('TITULAR DE LA PÓLIZA', 22, 139);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Tipo de persona: ", 20, 160);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(tipo_persona_cliente, 120, 160);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Razón Social: ", 20, 175);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(razon_social, 120, 175);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("RFC: ", 20, 190);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(rfc, 120, 190);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Calle: ", 20, 205);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(calle, 120, 205);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("No. Ext.: ", 20, 220);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(no_exterior, 120, 220);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("No. Int.: ", 220, 220);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(no_interior, 300, 220);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Colonia: ", 20, 235);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(colonia, 120, 235);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Estado: ", 20, 250);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(estado_cliente, 120, 250);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Ciudad: ", 220, 250);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(municipio_cliente, 300, 250);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("CP: ", 380, 250);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(codigo_postal, 425, 250);

                doc.rect(20, 265, 567, 18)
                    .fillAndStroke("#18244b", "#18244b")

                doc.font('ArialBold', 9)
                    .fill("white")
                    .fontSize(10)
                    .text('VIGENCIA', 22, 268);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Fecha Desde: ", 20, 290);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(fecha_salida, 120, 288);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Fecha Hasta: ", 220, 288);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(fecha_aproximada_llegada, 300, 288);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Moneda: ", 380, 288);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(tipo_moneda, 425, 288);

                doc.rect(20, 310, 567, 18)
                    .fillAndStroke("#18244b", "#18244b")

                doc.font('ArialBold', 9)
                    .fill("white")
                    .fontSize(10)
                    .text('BENEFICIARIO PREFERENTE', 22, 313);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Tipo de persona: ", 20, 333);

                if(tipo_persona_beneficiario === '3'){
                  doc.font('Arial', 10)
                      .fill("#18244b")
                      .fontSize(10)
                      .text('NINGUNO', 120, 333)
                }else{
                  doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(tipo_persona_beneficiario, 120, 333);
                }

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Nombre: ", 20, 350);

                if(nombre_beneficiario === '0'){
                    doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("NINGUNO", 120, 350);
                }else{
                    doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(nombre_beneficiario, 120, 350);
                }

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Bienes: ", 20, 365);

                if(bienes_beneficiario === '0'){
                    doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("NINGUNO", 120, 365);
                }else{
                    doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(bienes_beneficiario, 120, 365);
                }

                doc.rect(20, 385, 567, 18)
                    .fillAndStroke("#18244b", "#18244b")

                doc.font('ArialBold', 9)
                    .fill("white")
                    .fontSize(10)
                    .text('EMBARQUE', 22, 388);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Medio de Transporte: ", 20, 410);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(medio_transporte, 146, 410);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Fecha Salida: ", 20, 425);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(fecha_salida, 146, 425);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Fecha Llegada: ", 220, 425);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(fecha_aproximada_llegada, 300, 425);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Origen: ", 20, 440);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(pais_origen+', '+(estado_origen ? estado_origen : otro_estado_origen)+', '+(municipio_origen ? municipio_origen : otro_municipio_origen), 146, 440);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Destino: ", 20, 455);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(pais_destino+', '+(estado_destino ? estado_destino : otro_estado_destino)+', '+(municipio_destino ? municipio_destino : otro_municipio_destino), 146, 455);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Documento que ampara", 20, 470);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Talón de embarque:", 220, 470);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(no_embarque, 330, 470);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Transportista:", 220, 485);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(transportista, 330, 485);

                doc.rect(20, 505, 567, 18)
                    .fillAndStroke("#18244b", "#18244b")

                doc.font('ArialBold', 9)
                    .fill("white")
                    .fontSize(10)
                    .text('BIENES ASEGURADOS', 22, 508);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Descripción:", 20, 527);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(detalle_del_bien, 135, 527);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Tipo de embarque:", 20, 553);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(tipo_embarque, 135, 553);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Resp. Máxima:", 20, 567);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("$"+valor_del_embarque.toLocaleString()+'.00', 135, 567);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Sum Asegurada:", 250, 567);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("$"+valor_del_embarque.toLocaleString()+'.00', 335, 567);

                if(mostrar_prima === '1'){
                    doc.font('ArialBold', 12)
                        .fill("#18244b")
                        .fontSize(10)
                        .text("Prima Total:", 410, 567);

                    doc.font('Arial', 10)
                        .fill("#18244b")
                        .fontSize(10)
                        .text("$"+prima_total.toLocaleString(), 470, 567);
                }

                doc.rect(20, 595, 567, 18)
                    .fillAndStroke("#18244b", "#18244b")

                doc.font('ArialBold', 9)
                    .fill("white")
                    .fontSize(10)
                    .text('DESC. Y REC. /DEDUCIBLES', 22, 598);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Nombre de la Cobertura:", 20, 620);

                doc.font('ArialBold', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Deducibles", 300, 620);

                doc.font('Arial', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Riesgos Ordinarios de Transito (ROT)", 20, 635);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(deducibleUno, 300, 635);

                doc.font('Arial', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Robo de Bulto Por entero", 20, 650);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(deducibleDos, 300, 650);

                doc.font('Arial', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Robo Parcial", 20, 665);

                doc.font('Arial', 10)
                    .fill("#18244b")
                    .fontSize(10)
                    .text(deducibleTres, 300, 665);

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(10)
                    .text("Sujeto a las siguientes medidas de seguridad:  Custodia "+"_"+(custodia == 1 ? "X": "_")+"_ "+"Rastreo Satelital _"+(rastreo_satelital == 1 ? "X" : "_")+"_", 20, 680)

                /*
                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(8)
                    .text("Este documento no deberá presentar tachaduras o enmendadoras, en su caso perderá validez.", 20, 673)

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(8)
                    .text("Esta orden de trabajo ha sido elaborada considerando que no existe siniestro ni situaciones que pudieran dar origen a un siniestro por este embarque, en caso contrario, esta orden de trabajo no tendrá validez y se considerará nula y sin efecto.", 20, 684, {
                        align: 'justify'
                    })

                doc.font('ArialBold', 12)
                    .fill("#18244b")
                    .fontSize(8)
                    .text("La presente constancia de aseguramiento la otorga HDI Seguros S.A. de C.V. de conformidad y en base a las condiciones generales y particulares de las pólizas No. 391 y 392, teniendo prelación las segundas sobre las primeras, cualquier bien excluido o restringido en la póliza o el incumplimiento de las medidas de seguridad de la misma anularán la cobertura.", 20, 703, {
                        align: 'justify'
                    })
                */
                doc.addPage({
                    margins : {
                        top: 72,
                        left: 72,
                        right: 27,
                        bottom: 55
                    }
                })

                doc.rect(20, 100, 567, 18)
                    .fillAndStroke("#18244b", "#18244b")

                doc.font('ArialBold', 9)
                    .fill("white")
                    .fontSize(10)
                    .text('CLÁUSULAS', 22, 104);

                doc.font('Arial')
                    .fill("black")
                    .fontSize(9)
                    .text(clausulas, 20, 125, {
                    align: 'justify'
                })

                const range = doc.bufferedPageRange();
                for (let i = range.start; i <= (doc._pageBufferStart + doc._pageBuffer.length - 1); i++) {

                    doc.switchToPage(i);

                    doc.image('static/img/LogoInte-Group.jpg', 20, 20, { height:50, width:213 });
                    doc.image('static/img/LogoCargoTranspHD.png', 240, 10, { height:80, width:80 });
                    doc.image('static/img/ISOFINAL.png', 335, 20, { height:67, width:82 });

                    doc.font('ArialBold', 10)
                        .fill("#18244b")
                        .fontSize(11)
                        .text("Página " + (i + 1)  + " De " + range.count , 509, 55);

                    doc.font('ArialBold', 10)
                        .fill("#18244b")
                        .fontSize(11)
                        .text(fecha_actual, 475, 70);

                    doc.font('Arial', 12)
                        .fontSize(7)
                        .text("Inte-Group Agente de Seguros y Fianzas S.A de C.V.", 20, 740,
                        {lineBreak: false}
                    )

                    doc.font('Arial', 12)
                        .fontSize(7)
                        .text("Gdl: (33) 14 54 94 95", 20, 748,
                        {lineBreak: false}
                    )

                    doc.font('Arial', 12)
                        .fontSize(7)
                        .text("Col: (312) 454 10 30", 100, 748,
                        {lineBreak: false}
                    )

                    doc.font('Arial', 12)
                        .fontSize(7)
                        .text("Mzo: (314) 333 66 26", 180, 748,
                        {lineBreak: false}
                    )
                }

                doc.pipe(res);
                doc.end();
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })
        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })

    router.get('/consultar/todas/constancias/:tokenUser', (req, res, next) => {
        var tokenUser = req.params.tokenUser

        if (tokenUser === token) {
            var queryUsuarios =
                `
                    SELECT DISTINCT
                        bcons.id_constancia,
                        causer.usuario_nombre,
                        ccli.nombre,
                        ccli.apellido_paterno,
                        ccli.apellido_materno,
                        ccli.razon_social,
                        ccli.rfc,
                        bcons.prima_neta_total,
                        bcons.iva,
                        bcons.prima_total,
                        bcons.fecha_salida,
                        bcons.fecha_aproximada_llegada,
                        bcons.bmostrar_prima,
                        bcons.estatus_constancia
                    FROM bitconstancia bcons
                    LEFT JOIN catusuarios causer ON causer.id_usuario = bcons.id_usuario
                    LEFT JOIN catclientes ccli ON ccli.id_cliente = bcons.id_cliente
                    LEFT JOIN cattipopersona ctipcli ON ctipcli.id_tipo_persona = ccli.id_tipo_persona
                    LEFT JOIN cattipopersona ctip ON ctip.id_tipo_persona = bcons.id_tipo_persona_beneficiario
                    LEFT JOIN catbienesasegurados cbiena ON cbiena.id_bien_asegurado = bcons.id_bien_asegurado
                    LEFT JOIN cattipoembarque cemba ON cemba.id_tipo_embarque = bcons.id_tipo_embarque
                    LEFT JOIN cattipomoneda ctimon ON ctimon.id_tipo_moneda = bcons.id_tipo_moneda
                    LEFT JOIN catmediotransporte cmeditran ON cmeditran.id_medio_transporte = bcons.id_medio_transporte
                    LEFT JOIN paises pai ON pai.id_pais = bcons.id_pais_origen
                    LEFT JOIN estados estaccli ON estaccli.id = ccli.id_estado
                    LEFT JOIN estados esta ON esta.id = bcons.id_estado_origen
                    LEFT JOIN municipios muni ON muni.id = bcons.id_ciudad_origen
                    LEFT JOIN municipios municcli ON municcli.id = ccli.id_municipio
                    LEFT JOIN paises pais ON pais.id_pais = bcons.id_pais_destino
                    LEFT JOIN estados estad ON estad.id = bcons.id_estado_destino
                    LEFT JOIN municipios munic ON munic.id = bcons.id_ciudad_destino
                    ORDER BY bcons.id_constancia DESC
                `
            dbServer.query(queryUsuarios, { type: dbServer.QueryTypes.SELECT})
            .then(result => {
                res.json({status: 'OK', result: result})
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })
        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })

    router.get('/consultar/todas/constancias/cliente/:tokenUser/:id_usuario', (req, res, next) => {
        const {
            tokenUser,
            id_usuario
        } = req.params;

        if (tokenUser === token) {
            var queryUsuarios =
                `
                    SELECT DISTINCT
                        bcons.id_constancia,
                        causer.usuario_nombre,
                        ccli.nombre,
                        ccli.apellido_paterno,
                        ccli.apellido_materno,
                        ccli.razon_social,
                        ccli.rfc,
                        bcons.prima_neta_total,
                        bcons.iva,
                        bcons.prima_total,
                        bcons.fecha_salida,
                        bcons.fecha_aproximada_llegada,
                        bcons.bmostrar_prima,
                        bcons.estatus_constancia
                    FROM bitconstancia bcons
                    LEFT JOIN catusuarios causer ON causer.id_usuario = bcons.id_usuario
                    LEFT JOIN catclientes ccli ON ccli.id_cliente = bcons.id_cliente
                    LEFT JOIN cattipopersona ctipcli ON ctipcli.id_tipo_persona = ccli.id_tipo_persona
                    LEFT JOIN cattipopersona ctip ON ctip.id_tipo_persona = bcons.id_tipo_persona_beneficiario
                    LEFT JOIN catbienesasegurados cbiena ON cbiena.id_bien_asegurado = bcons.id_bien_asegurado
                    LEFT JOIN cattipoembarque cemba ON cemba.id_tipo_embarque = bcons.id_tipo_embarque
                    LEFT JOIN cattipomoneda ctimon ON ctimon.id_tipo_moneda = bcons.id_tipo_moneda
                    LEFT JOIN catmediotransporte cmeditran ON cmeditran.id_medio_transporte = bcons.id_medio_transporte
                    LEFT JOIN paises pai ON pai.id_pais = bcons.id_pais_origen
                    LEFT JOIN estados estaccli ON estaccli.id = ccli.id_estado
                    LEFT JOIN estados esta ON esta.id = bcons.id_estado_origen
                    LEFT JOIN municipios muni ON muni.id = bcons.id_ciudad_origen
                    LEFT JOIN municipios municcli ON municcli.id = ccli.id_municipio
                    LEFT JOIN paises pais ON pais.id_pais = bcons.id_pais_destino
                    LEFT JOIN estados estad ON estad.id = bcons.id_estado_destino
                    LEFT JOIN municipios munic ON munic.id = bcons.id_ciudad_destino
                    LEFT JOIN relusuariocliente reluscli ON reluscli.id_usuario = bcons.id_usuario
                    WHERE bcons.id_cliente IN (SELECT id_cliente FROM relusuariocliente WHERE id_usuario = ${id_usuario})
                    ORDER BY bcons.id_constancia DESC
                `
            dbServer.query(queryUsuarios, { type: dbServer.QueryTypes.SELECT })
            .then(result => {
                res.json({status: 'OK', result: result})
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })
        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })

    router.get('/imprimir/reporte/constancias/:tokenUser/:fecha_inicio/:fecha_termino', (req, res, next) => {
        var tokenUser = req.params.tokenUser
        var fecha_inicio = req.params.fecha_inicio
        var fecha_termino = req.params.fecha_termino

        if(tokenUser === token){
            var queryUsuarios =
                `
                    SELECT
                        bcons.id_constancia,
                        bcons.bmostrar_prima,
                        causer.usuario_nombre,
                        ccli.nombre,
                        ccli.apellido_paterno,
                        ccli.apellido_materno,
                        ccli.razon_social,
                        ccli.rfc,
                        ccli.calle,
                        ccli.no_exterior,
                        ccli.no_interior,
                        ccli.colonia,
                        ccli.deducible1,
                        ccli.deducible2,
                        ccli.deducible3,
                        estaccli.estado AS estado_cliente,
                        municcli.municipio AS municipio_cliente,
                        ccli.codigo_postal,
                        ctip.tipo_persona,
                        ctipcli.tipo_persona AS tipo_persona_cliente,
                        cbiena.bien_asegurado,
                        cemba.tipo_embarque,
                        ctimon.tipo_moneda,
                        cmeditran.medio_transporte,
                        pai.nombre AS pais_origen,
                        esta.estado,
                        bcons.otro_estado_origen,
                        muni.municipio,
                        bcons.otro_municipio_origen,
                        pais.nombre AS pais_destino,
                        estad.estado AS estado_destino,
                        bcons.otro_estado_destino,
                        munic.municipio AS municipio_destino,
                        bcons.otro_municipio_destino,
                        bcons.nombre_beneficiario,
                        bcons.bienes_beneficiario,
                        bcons.prima_neta_total,
                        bcons.otros_gastos,
                        bcons.iva,
                        bcons.prima_total,
                        bcons.detalle_del_bien,
                        bcons.valor_del_embarque,
                        bcons.no_embarque,
                        bcons.transportista,
                        bcons.rastreo_satelital,
                        bcons.custodia,
                        bcons.estatus_constancia,
                        bcons.tipo_compania,
                        bcons.NCertProv,
                        bcons.nombre_agrupador,
                        bcons.fecha_salida,
                        bcons.fecha_aproximada_llegada,
                        bcons.fecha_creacion
                    FROM bitconstancia bcons
                    LEFT JOIN catusuarios causer ON causer.id_usuario = bcons.id_usuario
                    LEFT JOIN catclientes ccli ON ccli.id_cliente = bcons.id_cliente
                    LEFT JOIN cattipopersona ctipcli ON ctipcli.id_tipo_persona = ccli.id_tipo_persona
                    LEFT JOIN cattipopersona ctip ON ctip.id_tipo_persona = bcons.id_tipo_persona_beneficiario
                    LEFT JOIN catbienesasegurados cbiena ON cbiena.id_bien_asegurado = bcons.id_bien_asegurado
                    LEFT JOIN cattipoembarque cemba ON cemba.id_tipo_embarque = bcons.id_tipo_embarque
                    LEFT JOIN cattipomoneda ctimon ON ctimon.id_tipo_moneda = bcons.id_tipo_moneda
                    LEFT JOIN catmediotransporte cmeditran ON cmeditran.id_medio_transporte = bcons.id_medio_transporte
                    LEFT JOIN paises pai ON pai.id_pais = bcons.id_pais_origen
                    LEFT JOIN estados estaccli ON estaccli.id = ccli.id_estado
                    LEFT JOIN estados esta ON esta.id = bcons.id_estado_origen
                    LEFT JOIN municipios muni ON muni.id = bcons.id_ciudad_origen
                    LEFT JOIN municipios municcli ON municcli.id = ccli.id_municipio
                    LEFT JOIN paises pais ON pais.id_pais = bcons.id_pais_destino
                    LEFT JOIN estados estad ON estad.id = bcons.id_estado_destino
                    LEFT JOIN municipios munic ON munic.id = bcons.id_ciudad_destino
                    LEFT JOIN cattipocompania cticom ON cticom.id_tipo_compania = bcons.id_tipo_compania
                    WHERE bcons.fecha_creacion BETWEEN '${fecha_inicio}' AND '${fecha_termino}'
                `
            dbServer.query(queryUsuarios, { type: dbServer.QueryTypes.SELECT})
            .then(result => {

                const fecha_creacion = moment(new Date()).format("YYYY-MM-DD")
                // res.json({status: 'OK', result: result})

                const styles = {
                    headerWisky:{
                        fill:{
                            fgColor: {
                                rgb: '00286C'
                            }
                        },
                        font: {
                            color: {
                            rgb: 'FFFFFFFF'
                            },
                            sz: 12,
                            bold: true,
                            underline: false
                        }
                    },
                    headerBold:{
                        fill:{
                            fgColor: {
                                rgb: 'ffffff'
                            }
                        },
                        font: {
                            color: {
                            rgb: '000'
                            },
                            sz: 12,
                            bold: true,
                            underline: false
                        }
                    },
                    headerDark: {
                    fill: {
                        fgColor: {
                        rgb: 'FF000000'
                        }
                    },
                    font: {
                        color: {
                        rgb: 'FFFFFFFF'
                        },
                        sz: 14,
                        bold: true,
                        underline: true
                        }
                    },
                    cellPink: {
                        fill: {
                            fgColor: {
                            rgb: 'FFFFCCFF'
                            }
                        }
                    },
                    cellGreen: {
                        fill: {
                            fgColor: {
                            rgb: '868A08'
                            }
                        },
                        font: {
                            color: {
                            rgb: 'FFFFFFFF'
                            },
                            sz: 12,
                            bold: true,
                            underline: false
                        }
                    }
                }

                const localLocale = moment();
                moment.locale('es');
                localLocale.locale(false);
                const fecha_hoy = localLocale.format('LLLL')

                const heading = [
                    [
                        {   value: 'InteGroup - Resumen de Constancias',
                            style: styles.headerWisky
                        },
                    ],
                    [
                        {   value: 'Resumen del Día: ' + fecha_hoy,
                            style: styles.headerBold
                        }
                    ]
                ];

                const specification = {
                    id_constancia: {
                        displayName: 'ID Constancia',
                        headerStyle: styles.headerWisky,
                        width: 50,
                    },
                    usuario_nombre: {
                        displayName: 'Usuario',
                        headerStyle: styles.headerWisky,
                        width: 150
                    },
                    nombre: {
                        displayName: 'Nombre',
                        headerStyle: styles.headerWisky,
                        width: 150
                    },
                    apellido_paterno: {
                        displayName: 'Apellido paterno',
                        headerStyle: styles.headerWisky,
                        width: 150
                    },
                    apellido_materno: {
                        displayName: 'Apellido materno',
                        headerStyle: styles.headerWisky,
                        width: 150
                    },
                    fecha_creacion: {
                        displayName: 'Fecha de creacion',
                        headerStyle: styles.headerWisky,
                        width: 150,
                        cellFormat: function(value, row) {
                            return (value != null ? moment(value).utc().format("DD/MM/YYYY") : '')
                        }, 
                    },
                    fecha_salida: {
                        displayName: 'Fecha de salida',
                        headerStyle: styles.headerWisky,
                        width: 150,
                        cellFormat: function(value, row) {
                            return (value != null ? moment(value).utc().format("DD/MM/YYYY") : '')
                        },
                    },
                    fecha_aproximada_llegada: {
                        displayName: 'Fecha aproximada de llegada',
                        headerStyle: styles.headerWisky,
                        width: 150,
                        cellFormat: function(value, row) {
                            return (value != null ? moment(value).utc().format("DD/MM/YYYY") : '')
                        },
                    },
                    estatus_constancia: {
                        displayName: 'Estatus constancia',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    razon_social: {
                        displayName: 'Razon social',
                        headerStyle: styles.headerWisky,
                        width: 150,
                    },
                    rfc: {
                        displayName: 'RFC',
                        headerStyle: styles.headerWisky,
                        width: 150,
                    },
                    calle: {
                        displayName: 'Calle',
                        headerStyle: styles.headerWisky,
                        width: 150,
                    },
                    no_exterior: {
                        displayName: 'No Exterior',
                        headerStyle: styles.headerWisky,
                        width: 150,
                    },
                    no_interior: {
                        displayName: 'No Interior',
                        headerStyle: styles.headerWisky,
                        width: 150,
                    },
                    colonia: {
                        displayName: 'Colonia',
                        headerStyle: styles.headerWisky,
                        width: 150,
                    },
                    deducible1: {
                        displayName: 'Deducible uno',
                        headerStyle: styles.headerWisky,
                        width: 150,
                    },
                    deducible2: {
                        displayName: 'Deducible dos',
                        headerStyle: styles.headerWisky,
                        width: 150,
                    },
                    deducible3: {
                        displayName: 'Deducible tres',
                        headerStyle: styles.headerWisky,
                        width: 150,
                    },
                    estado_cliente: {
                        displayName: 'Estado cliente',
                        headerStyle: styles.headerWisky,
                        width: 150,
                    },
                    municipio_cliente: {
                        displayName: 'Municipio cliente',
                        headerStyle: styles.headerWisky,
                        width: 150,
                    },
                    codigo_postal: {
                        displayName: 'Codigo postal',
                        headerStyle: styles.headerWisky,
                        width: 150,
                    },
                    tipo_persona: {
                        displayName: 'Tipo de persona',
                        headerStyle: styles.headerWisky,
                        width: 150,
                    },
                    tipo_persona_cliente: {
                        displayName: 'Tipo persona cliente',
                        headerStyle: styles.headerWisky,
                        width: 150,
                    },
                    bien_asegurado: {
                        displayName: 'Bien asegurado',
                        headerStyle: styles.headerWisky,
                        width: 150,
                    },
                    tipo_embarque: {
                        displayName: 'Tipo embarque',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    tipo_moneda: {
                        displayName: 'Tipo de moneda',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    medio_transporte: {
                        displayName: 'Medio de transporte',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    pais_origen: {
                        displayName: 'Pais Origen',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    estado: {
                        displayName: 'Estado',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    otro_estado_origen: {
                        displayName: 'Otro estado origen',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    municipio: {
                        displayName: 'Municipio',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    otro_municipio_origen: {
                        displayName: 'Otro municipio origen',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    pais_destino: {
                        displayName: 'Pais destino',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    estado_destino: {
                        displayName: 'Estado destino',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    otro_estado_destino: {
                        displayName: 'Otro estado destino',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    municipio_destino: {
                        displayName: 'Municipio destino',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    otro_municipio_destino: {
                        displayName: 'Otro municipio destino',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    nombre_beneficiario: {
                        displayName: 'Nombre beneficiario',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    bienes_beneficiario: {
                        displayName: 'Bienes beneficiario',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    prima_neta_total: {
                        displayName: 'Prima neta total',
                        headerStyle: styles.headerWisky,
                        width: 150,
                    },
                    otros_gastos: {
                        displayName: 'Otros gastos',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    iva: {
                        displayName: 'I.V.A.',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    prima_total: {
                        displayName: 'Prima total',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    detalle_del_bien: {
                        displayName: 'Detalle del bien',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    valor_del_embarque: {
                        displayName: 'Valor del embarque',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    no_embarque: {
                        displayName: 'No embarque',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    transportista: {
                        displayName: 'Transportista',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    rastreo_satelital: {
                        displayName: 'Rastreo satelital',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    custodia: {
                        displayName: 'Custodia',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    tipo_compania: {
                        displayName: 'Tipoo de compañia',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    NCertProv: {
                        displayName: 'NCertProv',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                    nombre_agrupador: {
                        displayName: 'Nombre agrupador',
                        headerStyle: styles.headerWisky,
                        width: 150, 
                    },
                }
                
                const dataset = result

                const merges = [
                    { start: { row: 1, column: 1 }, end: { row: 1, column: 10 } },
                    { start: { row: 2, column: 1 }, end: { row: 2, column: 5 } },
                    { start: { row: 2, column: 6 }, end: { row: 2, column: 10 } }
                ]

                const report = excel.buildExport(
                    [
                        {
                            name: 'Reporte de Constancias',
                            heading: heading,
                            merges: merges,
                            specification: specification,
                            data: dataset
                        }
                    ]
                )

                res.attachment('reporte_'+fecha_creacion+'.xlsx');
                return res.send(report);
            })
            .catch(error => {
                res.json({status: 'ERROR', result: error})
            })
        }else{
            res.json({ status: 'ERROR', response: 'No se ha iniciado sesión'})
        }
    })

    router.post('/cancelar/estatus/constancia', (req, res, next) => {
        var tokenUser = req.body.tokenUser
        var id_constancia = req.body.id_constancia

        if(tokenUser === token){

            var actualizar = `
                UPDATE bitconstancia
                SET estatus_constancia = 'CANCELADA'
                WHERE id_constancia = ${id_constancia}
            `

            dbServer.query(actualizar, { type: dbServer.QueryTypes.UPDATE })
            .then(result => {
                res.json({ status: 'OK', result: result })
            })
            .catch(error => {
                res.json({ status: 'ERROR', result: error })
            })

        }else{
            res.json({ status: 'ERROR', result: 'No has iniciado sesión' })
        }
    })

    router.post('/activar/estatus/constancia', (req, res, next) => {
        var tokenUser = req.body.tokenUser
        var id_constancia = req.body.id_constancia

        if(tokenUser === token){

            var actualizar = `
                UPDATE bitconstancia
                SET estatus_constancia = 'ACTIVA'
                WHERE id_constancia = ${id_constancia}
            `

            dbServer.query(actualizar, { type: dbServer.QueryTypes.UPDATE })
            .then(result => {
                res.json({ status: 'OK', result: result })
            })
            .catch(error => {
                res.json({ status: 'ERROR', result: error })
            })

        }else{
            res.json({ status: 'ERROR', result: 'No has iniciado sesión' })
        }
    })

    router.get('/actualizar/mostrar/prima/:tokenUser/:id_constancia/:mostrar_prima', (req, res,next) => {
        let tokenUser = req.params.tokenUser
        let id_constancia = req.params.id_constancia
        let mostrar_prima = req.params.mostrar_prima

        if(tokenUser === token){

            var actualizar = `
              UPDATE bitconstancia
              SET bmostrar_prima = ${mostrar_prima}
              WHERE id_constancia = ${id_constancia}
            `

            dbServer.query(actualizar, { type: dbServer.QueryTypes.UPDATE })
            .then(result => {
                res.json({ status: 'OK', result: result })
            })
            .catch(error => {
                res.json({ status: 'ERROR', result: error })
            })

        }else{
            res.json({ status: 'ERROR', result: 'No has iniciado sesión' })
        }
    })

    function pad(num, size) {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }

    router.post('/revisar/numero/embarque', (req, res, next) => {
        var tokenUser = req.body.tokenUser
        var no_embarque = req.body.no_embarque

        if(tokenUser === token){

            var select = `
                SELECT no_embarque
                FROM bitconstancia
                WHERE no_embarque = '${no_embarque}'
            `

            dbServer.query(select, { type: dbServer.QueryTypes.SELECT })
            .then(result => {
                res.json({ status: 'OK', embarque: result })
            })
            .catch(error => {
                res.json({ status: 'ERROR', result: error })
            })

        }else{
            res.json({ status: 'ERROR', result: 'No has iniciado sesión' })
        }
    })
}
