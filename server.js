const compression   = require('compression')
const express       = require('express')
const app           = express()
const router        = express.Router()
const bodyParser    = require("body-parser")
const excel         = require('node-excel-export');
const here          = __dirname
const Sequelize     = require('sequelize')
const md5           = require('md5')
const fs            = require('fs')
const PDFDocument   = require('pdfkit')
const moment        = require('moment')
const knex          = require('./knexconfig');
const uniqid	 			= require('uniqid');
const axios					= require('axios');

require('dotenv').config();

/* Configuramos express server */
app.use(compression())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))

app.disable('x-powered-by')
app.use(function(req, res, next){
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next()
})

router.use(function(req, res, next){
    console.log("[ API ] -> /" + req.method + ' -> ' + req.path)
    next()
})

/* configuracion de base de datos */
const dbServer = new Sequelize(
	process.env.DB_NAME,
	process.env.DB_USER,
	process.env.DB_PASS,
	{
			host: process.env.DB_HOST,
			dialect: 'mysql',
			operatorsAliases: false,
			timezone: '-05:00',
			logging: false,
			pool: {
					max: 5,
					min: 0,
					idle: 10000
	}
});

const token = md5('!NT3RG$R0UPAP!');

/* API Routes */
require('./routes/server-status')(router, dbServer, token, knex);
require ('./routes/control-usuarios')(router, dbServer, token, md5);
require ('./routes/constancia')(router, dbServer, token, md5, fs, PDFDocument, moment, excel, knex, uniqid);
require ('./routes/usuarios')(router, dbServer, token, md5, knex);
require ('./routes/clientes')(router, dbServer, token, md5);
require ('./routes/companias')(router, dbServer, token, md5, fs, PDFDocument, moment, knex);
require ('./routes/pagos')(router, dbServer, token, md5, fs, PDFDocument, moment, knex, uniqid, axios);


/* Inicio de server */
const port = process.env.PORT || 6060

app.use("/api", router);

app.use("*", function(req, res){
  res.status(404).json({status: 'ERROR', result: '404'})
});

app.listen(port, function(){
  console.log('[ API ] -> Servidor Listo - Port: ' + port)
});
